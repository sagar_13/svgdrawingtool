import { Component, OnInit } from '@angular/core';
import { ShapeComponent } from '../shape/shape.component';
import {DatabaseBox, MousePosition } from '../../model/shape';
import { ShapeType } from '../../model/shape-types';
import { WIDTH, HEIGHT } from '../../model/shape-size';
import { Field } from 'dynaform';

@Component({
    selector: 'app-db',
    templateUrl: './db.component.html',
    styleUrls: ['./db.component.css']
})
export class DbComponent extends ShapeComponent implements OnInit {
    formFields: Field[] = [
        {
            name: 'x',
            label: 'X:',
            type: 'input',
            inputType: 'text',
            value: ''
        },
    ];

    constructor() {
        super();
        this.shape = new DatabaseBox();
        this.shapeType = ShapeType.Database;
    }

    ngOnInit() {
    }

    startDrawing(beginPosition: MousePosition): void {
        if (this.shape instanceof DatabaseBox) {
            this.shape.originX = beginPosition.x;
            this.shape.originY = beginPosition.y;
        }
    }

    draw(currentPosition: MousePosition): void {
        if (this.shape instanceof DatabaseBox) {
            this.shape.width = WIDTH; //Math.abs(currentPosition.x - this.shape.originX);
            this.shape.height = HEIGHT;//Math.abs(currentPosition.y - this.shape.originY);
        }
    }

}
