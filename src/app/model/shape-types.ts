export enum ShapeType {
    NoShape,
    Line,
    Circle,
    Ellipse,
    Rectangle,
    TextBox,
    Path,
    PolyLine,
    Square,
    Image,
    Pdf,
    Docx,
    Csv,
    Database
}

export enum ToolType {
    Pointer,
    Move,
    Rotate,
    SelectArea,
    Flipvertical,
    Fliphorizontal
}

export enum State {
    None,
    Moving,
    Finished

}